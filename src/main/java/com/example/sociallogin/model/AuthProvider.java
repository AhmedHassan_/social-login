package com.example.sociallogin.model;

public enum  AuthProvider {
    local,
    facebook,
    google,
    github
}
